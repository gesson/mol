package sma;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.gui.GuiEvent;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sma.agent.ConsomateurAgent;

public class ConsomateurContainer extends Application{
	private ConsomateurAgent consomateurAgent;
	private ObservableList<String> observableList;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(ConsomateurContainer.class);
	}

	public void startContainer() {
		try {
			Runtime runtime=Runtime.instance();
			Profile profile=new ProfileImpl(false);
			profile.setParameter(Profile.MAIN_HOST, "localhost");
			AgentContainer agentContainer=runtime.createAgentContainer(profile);
			AgentController agentController=agentContainer.createNewAgent("consomateur", "sma.agent.ConsomateurAgent", new Object[] {this});
			agentController.start();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		startContainer();
		
		primaryStage.setTitle("Consomateur");
		BorderPane borderPane=new BorderPane();
		
		HBox hBox=new HBox();
		hBox.setPadding(new Insets(10));
		hBox.setSpacing(10);
		Label labelProduit=new Label("Produit:");
		TextField txtProduit=new TextField();
		Button btnAcheter=new Button("Acheter");
		TextField txtNote=new TextField();
		Button btnNote=new Button("evaluer");
		hBox.getChildren().add(labelProduit);
		hBox.getChildren().add(txtProduit);
		hBox.getChildren().add(btnAcheter);
		
		borderPane.setTop(hBox);
		VBox vBox=new VBox();
		GridPane gridPane=new GridPane();
		hBox.getChildren().add(txtNote);
		hBox.getChildren().add(btnNote);
		observableList=FXCollections.observableArrayList();
		ListView<String> lstViewMessage=new ListView<String>(observableList);
		
		gridPane.add(lstViewMessage, 0, 0);
		vBox.setPadding(new Insets(10));
		vBox.setSpacing(10);
		vBox.getChildren().add(gridPane);
		borderPane.setCenter(vBox);
		Scene scene=new Scene(borderPane,400,500);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		btnAcheter.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				String produit=txtProduit.getText();
				GuiEvent guiEvent=new GuiEvent(this, 1);
				guiEvent.addParameter(produit);
				consomateurAgent.onGuiEvent(guiEvent);
			}
			
		});
		
		btnNote.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				
//				String note=txtNote.getText();
//				GuiEvent guiEvent2=new GuiEvent(this, 1 );
//				guiEvent2.addParameter(note);
//				consomateurAgent.onGuiEvent(guiEvent2);
			}
			
		});
	}

	public void viewMessage(GuiEvent guiEvent) {
		String message=guiEvent.getParameter(0).toString();
		String message1=guiEvent.getParameter(1).toString();
		observableList.add(message);
		observableList.add(message1);
	}
	
	public ConsomateurAgent getConsomateurAgent() {
		return consomateurAgent;
	}

	public void setConsomateurAgent(ConsomateurAgent consomateurAgent) {
		this.consomateurAgent = consomateurAgent;
	}
}
