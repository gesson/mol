package sma;

import jade.core.AID;
import jade.util.leap.Serializable;

public class Note implements Serializable {

	AID sender;
	double note;
	


	public Note(AID idAgVendeur, double note) {
		// TODO Auto-generated constructor stub
		this.sender = idAgVendeur;
		this.note = note;
	}

	public AID getSender() {
		return sender;
	}

	public void setSender(AID sender) {
		this.sender = sender;
	}

	public double getNote() { 
		return note;
	}

	public void setNote(double note) {
		this.note = note;
	}

	
	
	
}


