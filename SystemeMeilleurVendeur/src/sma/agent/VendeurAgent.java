 package sma.agent;

import java.util.ArrayList;
import java.util.Random;

import com.google.gson.Gson;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.df;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.ControllerException;
import sma.AcheteurContainer;
import sma.Produit;
import sma.VendeurContainer;

public class VendeurAgent extends GuiAgent{
	private VendeurContainer gui;
	@Override
	protected void setup() {
		gui=(VendeurContainer) getArguments()[0];
		gui.setVendeurAgent(this);
		
		System.out.print("Initialisation de l'agent "+this.getAID().getName());
		
		ParallelBehaviour parallelBehaviour=new ParallelBehaviour();
		addBehaviour(parallelBehaviour);
		
		parallelBehaviour.addSubBehaviour(new CyclicBehaviour() {
			
			@Override
			public void action() {
				ACLMessage aclMessage=receive();
				if(aclMessage!=null) {
					switch (aclMessage.getPerformative()) {
						case ACLMessage.CFP:
							GuiEvent guiEvent=new GuiEvent(this, 1);
							guiEvent.addParameter(aclMessage.getContent());
							gui.viewMessage(guiEvent);
							
							
							ACLMessage aclMessage1=new ACLMessage(ACLMessage.PROPOSE);
							
							//n, n1, n2 generer des prix aleatoires pour les produits
							Random rand =new Random();
							Random rand1 =new Random();
							Random rand2 =new Random();
							int n=(rand.nextInt(200)+10);
							int n1=(rand1.nextInt(200)+10);
							int n2=(rand1.nextInt(200)+10);
							int t[]= {n,n1,n2};
							//creation d'objet produit
							Produit produit0= new  Produit("java xml", t[0]);
							Produit produit1=new Produit("Romero et juliette", t[1]);
							Produit produit2=new Produit("Perle des antilles", t[2]);
							//creation d'ArrayList de type objet
							ArrayList<Produit> produit=new ArrayList<Produit>();
							//on ajoute les produits dans l'ArrayList
							produit.add(0,produit0);
							produit.add(1,produit1);
							produit.add(2,produit2);
							
							//on parcours l'ArrayList on compare le titre que l'agent acheteur
							//a envoye dans le message CFP si on le trouve on envoie un message
							// de type PROPOSE en Json
							for(int i=0; i<produit.size(); i++) {
								if(produit.get(i).getName().equalsIgnoreCase(aclMessage.getContent())) {
									Gson gson = new Gson();
									aclMessage1.setContent(gson.toJson(produit.get(i)));
									aclMessage1.addReceiver(new AID("acheteur",AID.ISLOCALNAME));
									send(aclMessage1);
								}
							}
							
							break; 
							
						case ACLMessage.ACCEPT_PROPOSAL:
							String message=aclMessage.getContent();
							System.out.println(message);
							
							break;
						default:
							break;
					}
				}
				else {
					block();
				}
			}
		});
		
		parallelBehaviour.addSubBehaviour(new OneShotBehaviour() {
			
			@Override
			public void action() {
				try {
					DFAgentDescription dfa=new DFAgentDescription();
					dfa.setName(getAID());
					ServiceDescription sd=new ServiceDescription();
					sd.setType("Vente");
					sd.setName("vente-produits");
					dfa.addServices(sd);
					DFService.register(myAgent, dfa);
				} catch (FIPAException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	@Override
	protected void takeDown() {
		System.out.println("Desctruction de l'agent");
	}

	@Override
	protected void beforeMove() {
		try {
			System.out.println("Avant migration ... du container "+this.getContainerController().getContainerName());
		} catch (ControllerException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void afterMove() {
			try {
				System.out.println("Apres migration ... vers le container "+this.getContainerController().getContainerName());
			} catch (ControllerException e) {
				e.printStackTrace();
			}
		}
	@Override
	public void onGuiEvent(GuiEvent guiEvent) {

	}
}



